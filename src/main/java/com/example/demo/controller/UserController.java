package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.UserService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class UserController {

	@Autowired
	private UserService user;

	@GetMapping("/")
	public List<User> listar() {
		return user.listar();

	}

	@GetMapping("/{id}")
	public User pesquisar(@PathVariable Long id) {
		return user.pesquisar(id);

	}

	@PostMapping()
	public User salvar(@RequestBody User us) {
		return user.salvar(us);

	}
	
	@PutMapping("/{id}")
	public User editar(@PathVariable Long id, @RequestBody User us) {
		return user.editar(id,us);

	}
	
	@DeleteMapping("/{id}")
	public User deletar(@PathVariable Long id) {
		return user.deletar(id);

	}

}
