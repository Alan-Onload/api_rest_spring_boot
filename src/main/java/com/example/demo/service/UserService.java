package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Service
@Transactional
public class UserService {
	@Autowired
	private UserRepository user;
	
	public List<User> listar(){
		return user.findAll();
	}
	
	public User pesquisar(Long id){
		return user.findById(id).orElse(null);
	}
	
	public Optional<User> pesquisarLogin(String username,String password){
		return user.findUserByUsernameAndPassword(username,password);
	}
	
	public User salvar(User us) {
		return user.save(us);
	}
	
	public User editar(Long id,User us) {
		User old = user.findById(id).orElse(null);
		if(old !=null) {
		old.setUsername(us.getUsername());
		old.setPassword(us.getPassword());
		return user.save(old);
		}
		return user.save(new User());
	}
	public User deletar(Long id){
		User old = user.findById(id).orElse(null);
		user.delete(old);
		return old;
	}


}










